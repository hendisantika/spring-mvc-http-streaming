# Spring MVC HTTP Streaming

#### Things to do to run without Docker (locally):

1. Clone this project by this command : `git clone https://gitlab.com/hendisantika/spring-mvc-http-streaming.git`
2. Go to folder --> `cd spring-mvc-http-streaming`
3. Run this app by this command : `mvn clean spring-boot:run`

![Home Page](img/home.png "Home Page")
