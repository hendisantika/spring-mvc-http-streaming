package com.hendisantika.reactive.springmvchttpstreaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMvcHttpStreamingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringMvcHttpStreamingApplication.class, args);
    }

}

